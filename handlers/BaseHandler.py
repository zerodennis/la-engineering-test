import tornado
import ast
import datetime
import copy
import requests
import json
import settings as s
from tornado import web

class AppException(web.HTTPError):
    pass

class BaseHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Methods", "GET,OPTIONS,POST,DELETE")
        self.set_header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Authorization, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")

    def get(self):
        self.write('Running!')

    def write_error(self, status_code, **kwargs):

        self.set_header('Content-Type', 'application/json')
        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            self.finish(json.dumps({
                    'error': {
                            'code': status_code,
                            'message': self._reason,
                    }
            }))
        else:
            self.finish(json.dumps({
                'error': {
                    'code': status_code,
                    'message': self._reason,
                }
            }))

    def options(self):
        # no body
        self.set_status(204)
        self.finish()

    def authenticate_user(self):
        return 'sample-token'