from handlers.BaseHandler import BaseHandler
from handlers.BaseHandler import AppException
import json
import uuid

todo_list_items = []
class TodoListHandler(BaseHandler):
    def get(self):
        self.set_status(200)
        self.write({ 'todo_list':  todo_list_items })

class AddTodo(BaseHandler):
    def addTodo(self, details):
        todo_list_items.append({ 'id': str(uuid.uuid1()),
            'details': details })
        self.set_status(201)
        return todo_list_items

    def post(self):
        details = self.get_argument('description', default = '')
        response = {
            'todo_list': self.addTodo(details)
        }
        self.write(response)

class DeleteTodo(BaseHandler):
    def deleteTodo(self, id):
        global todo_list_items
        todo_index = next((index
            for (index, d) in enumerate(todo_list_items)
            if d['id'] == id), None)
        if (todo_index == None):
            raise AppException(status_code = 400, reason = 'Todo List item does not exist')
        todo_list_items.pop(todo_index)
        self.set_status(204)

    def delete(self):
        id = self.get_argument('id')
        response = {
            'todo_list': self.deleteTodo(id)
        }

class UpdateTodo(BaseHandler):
    def updateTodo(self, id, new_details):
        global todo_list_items
        found = False
        foundItem = None
        for item in todo_list_items:
            if (item['id'] == id):
                found = True
                item.update(details = new_details)
                foundItem = item

        if (found == False):
            raise AppException(status_code = 400, reason = 'Todo List item does not exist')

        self.set_status(200)
        return foundItem

    def patch(self):
        id = self.get_argument('id')
        details = self.get_argument('details')

        response = self.updateTodo(id, details)
        self.write(response)

class MoveTodo(BaseHandler):
    def moveTodo(self, id, new_index):
        global todo_list_items

        todo_index = next((index
            for (index, d) in enumerate(todo_list_items)
            if d['id'] == id), None)

        if (todo_index == None):
            raise AppException(status_code = 400, reason = 'Todo List item does not exist')
        elif (new_index > len(todo_list_items) or new_index < 0):
            raise AppException(status_code = 400, reason = 'Todo List index out of bounds')

        todo_list_items.insert(new_index, todo_list_items.pop(todo_index))
        self.set_status(200)
        return todo_list_items

    def patch(self):
        id = self.get_argument('id')
        index = self.get_argument('index')

        response = {
            'todo_list': self.moveTodo(id, int(index))
        }
        self.write(response)
