import tornado.ioloop
import tornado.httpserver
import tornado.web
import os
import base64
import traceback
from tornado import autoreload

from handlers.BaseHandler import BaseHandler
from handlers.TodoListHandler import TodoListHandler, AddTodo, DeleteTodo, UpdateTodo, MoveTodo

settings = {
            "debug": True,
            "serve_traceback": True
            }

application = tornado.web.Application([
    (r"/", BaseHandler),
    (r"/todolist", TodoListHandler),
    (r"/todolist/add", AddTodo),
    (r"/todolist/delete", DeleteTodo),
    (r"/todolist/update", UpdateTodo),
    (r"/todolist/move", MoveTodo)
], **settings)

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 7777))
    application.listen(port)
    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.start()